package com.scansione.easyimage.helper;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

/**
 * Created by ajijul on 22/12/16.
 */

public abstract class CachingParameters {
    public static int MEMORY_CACHE_SIZE = 1024 * 5;
    public static boolean IS_MEMORY_CACHE_ENABLED = true;

    public static int getCachingMemorySize(Context context) {
        int memClass = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        int cacheSize = 1024 * 1024 * memClass / 8;
        return cacheSize;
    }
}
