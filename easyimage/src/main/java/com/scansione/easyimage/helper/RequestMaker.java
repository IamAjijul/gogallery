package com.scansione.easyimage.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import com.scansione.easyimage.EasyImage;
import com.scansione.easyimage.logger.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by ajijul on 21/12/16.
 */

public class RequestMaker {

    private EasyImage easyImage;
    private Uri uri;
    private int resourceId = 0;
    private EasyImage.SetOnImageLoadListener setOnImageLoadListener;
    private ImageCaching imageCaching;


    public RequestMaker(EasyImage singletoneClass, Uri parse, int i
    ) {
        this.easyImage = singletoneClass;
        this.uri = parse;
        this.resourceId = i;

    }

    public void into(ImageView imageView, EasyImage.SetOnImageLoadListener listener) {
        if (imageCaching == null) {
            imageCaching = new ImageCaching();
            imageCaching.initMyCaching(imageView.getContext());
        }
        Logger.setLogTag("EASY IMAGE");
        setOnImageLoadListener = listener;
        if (resourceId != 0) {
            Logger.showInfoLog("RESOURCE ID");
        } else if (uri.toString().contains("http://") || uri.toString().contains("https://")) {
            Logger.showInfoLog("URI CONTAIN URL");
            new ImageLoader(imageView, uri, setOnImageLoadListener, imageCaching).execute();
        } else {
            try {

                Bitmap bitmap = imageCaching.getBitmapFromMemCache(uri.toString());
                if (bitmap != null)
                    imageView.setImageBitmap(bitmap);
                else {
                    bitmap = ImageProcessor.decodeUri(imageView.getContext(), Uri.parse("file://" + uri));
                    imageCaching.addBitmapToCache(uri.toString(), bitmap);
                }

                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }
}
