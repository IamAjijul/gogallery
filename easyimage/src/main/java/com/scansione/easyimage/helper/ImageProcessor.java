package com.scansione.easyimage.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;

/**
 * Created by ajijul on 23/12/16.
 */

public class ImageProcessor {
    public static int MAX_IMAGE_WIDTH = 100;
    public static int MAX_IMAGE_HEIGHT = 150;
    public static int requestedHeight;
    public static int requestedWidth;

    public static int resizingBitmap(int requestHeight, int requestWidth, Bitmap bitmap) {
        if (bitmap == null) {
            requestedHeight = requestHeight;
            requestedWidth = requestWidth;
        } else {
            requestedHeight = bitmap.getHeight();
            requestedWidth = bitmap.getWidth();
        }
        int inSampleSize = 1;

        if (requestedHeight > MAX_IMAGE_HEIGHT || requestedWidth > MAX_IMAGE_WIDTH) {

            final int halfHeight = requestedHeight / 2;
            final int halfWidth = requestedWidth / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > MAX_IMAGE_HEIGHT
                    && (halfWidth / inSampleSize) > MAX_IMAGE_WIDTH) {
                inSampleSize *= 2;
            }

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            long totalPixels = requestedWidth * requestedHeight / inSampleSize;

            // Anything more than 2x the requested pixels we'll sample down further
            final long totalReqPixelsCap = MAX_IMAGE_WIDTH * MAX_IMAGE_HEIGHT * 2;

            while (totalPixels > totalReqPixelsCap) {
                inSampleSize *= 2;
                totalPixels /= 2;
            }
        }
        return inSampleSize;
    }

    /**
     * Decode and sample down a bitmap from resources to the requested width and height.
     *
     * @param res       The resources object containing the image data
     * @param resId     The resource id of the image data
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     * that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight, Bitmap bitmap) {

        // BEGIN_INCLUDE (read_bitmap_dimensions)
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = resizingBitmap(reqHeight, reqWidth, bitmap);
        // END_INCLUDE (read_bitmap_dimensions)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     * Decode and sample down a bitmap from a file to the requested width and height.
     *
     * @param filename  The full path of the file to decode
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     * that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromFile(File filename,
                                                     int reqWidth, int reqHeight, Bitmap bitmap) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename.getName(), options);

        // Calculate inSampleSize
        options.inSampleSize = resizingBitmap(reqHeight, reqWidth, bitmap);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filename.getName(), options);
    }

    /**
     * Decode and sample down a bitmap from a file input stream to the requested width and height.
     *
     * @param fileDescriptor The file descriptor to read from
     * @param reqWidth       The requested width of the resulting bitmap
     * @param reqHeight      The requested height of the resulting bitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     * that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromDescriptor(
            FileDescriptor fileDescriptor, int reqWidth, int reqHeight, Bitmap bitmap) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);

        // Calculate inSampleSize
        options.inSampleSize = resizingBitmap(reqWidth, reqHeight, bitmap);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;


        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
    }

    public static Bitmap getResizedBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        while (true) {
            if (width < MAX_IMAGE_WIDTH || height < MAX_IMAGE_HEIGHT)
                break;
            float ratio = Math.min(
                    (float) MAX_IMAGE_WIDTH / bm.getWidth(),
                    (float) MAX_IMAGE_HEIGHT / bm.getHeight());
            width = Math.round((float) ratio * bm.getWidth());
            height = Math.round((float) ratio * bm.getHeight());
        }

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bm, width,
                height, true);
        return resizedBitmap;
    }

    public static Bitmap decodeUri(Context c, Uri uri)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < MAX_IMAGE_WIDTH || height_tmp / 2 < MAX_IMAGE_HEIGHT)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }
}
