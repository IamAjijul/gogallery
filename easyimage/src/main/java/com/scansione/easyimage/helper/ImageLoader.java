package com.scansione.easyimage.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;

import com.scansione.easyimage.EasyImage;
import com.scansione.easyimage.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ajijul on 21/12/16.
 */

public class ImageLoader extends AsyncTask<String, Integer, Bitmap> {
    private Uri uri;
    private EasyImage.SetOnImageLoadListener setOnImageLoadListener;
    private ImageView.ScaleType actualScalingMethod;
    private WeakReference<ImageView> viewReference;
    private ImageCaching imageCaching;

    public ImageLoader(ImageView imageView, Uri uri, EasyImage.SetOnImageLoadListener setOnImageLoadListener, ImageCaching imageCaching) {
        this.imageCaching = imageCaching;
        viewReference = new WeakReference<ImageView>(imageView);
        actualScalingMethod = imageView.getScaleType();
        imageView.setImageDrawable(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setImageDrawable(imageView.getContext().getResources().getDrawable(R.drawable.download, null));
        } else {
            imageView.setImageDrawable(imageView.getContext().getResources().getDrawable(R.drawable.download));
        }
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        this.uri = uri;
        this.setOnImageLoadListener = setOnImageLoadListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        ImageView imageView = viewReference.get();
        if (imageView != null) {
            if (bitmap == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imageView.setImageDrawable(imageView.getContext().getResources().getDrawable(R.drawable.exclamation, null));
                } else {
                    imageView.setImageDrawable(imageView.getContext().getResources().getDrawable(R.drawable.exclamation));
                }
                return;
            }
            imageView.setImageBitmap(bitmap);
            imageView.setScaleType(actualScalingMethod);

        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(Bitmap bitmap) {
        super.onCancelled(bitmap);
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        Bitmap bitmap = imageCaching.getBitmapFromMemCache(uri.toString());
        if (bitmap != null) {
            Log.e("ImageLoader", "BITMAP GOT FROM CACHE");
            return bitmap;
        }
        Log.e("ImageLoader", "BITMAP GOT FROM NETWORK");
        bitmap = downloadBitmap(uri);
        imageCaching.addBitmapToCache(uri.toString(), bitmap);
        return downloadBitmap(uri);
    }

    public Bitmap downloadBitmap(Uri url) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(url.toString()).openConnection();
            httpURLConnection.setUseCaches(true);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() >= 300) {
                if (setOnImageLoadListener != null)
                    setOnImageLoadListener.onImageLoadFailed("Network Error");
                return null;
            }
            InputStream inputStream = httpURLConnection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

            return ImageProcessor.getResizedBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
