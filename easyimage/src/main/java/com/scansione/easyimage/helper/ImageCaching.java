package com.scansione.easyimage.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.LruCache;

import com.scansione.easyimage.BuildConfig;

/**
 * Created by ajijul on 22/12/16.
 */

public class ImageCaching {

    private static final String TAG = "MemoryCache";
    public static LruCache<String, Bitmap> bitmapLruCache;

    public void initMyCaching(Context context) {
        int Ca = CachingParameters.getCachingMemorySize(context);
        Log.d(TAG, "INIT cache hit" + Ca);

        if (bitmapLruCache == null) {
            Log.d(TAG, "LRU CACHE CREATING............with size : " + (Ca / 1024));
            bitmapLruCache = new LruCache<String, Bitmap>(Ca) {
                @Override
                protected int sizeOf(String key, Bitmap value) {
                    return value.getByteCount();
                }

                @Override
                protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
                    super.entryRemoved(evicted, key, oldValue, newValue);
                    oldValue.recycle();
                }
            };
        }

    }

    /***
     * Add Cache to memory
     *
     * @param data  Key value
     * @param value bitmap value
     */

    public void addBitmapToCache(String data, Bitmap value) {
        //BEGIN_INCLUDE(add_bitmap_to_cache)
        if (data == null || value == null) {
            return;
        }
        // Add to memory cache
        if (bitmapLruCache != null) {
            bitmapLruCache.put(data, value);
        }
    }

    /**
     * Get from memory cache.
     *
     * @param data Unique identifier for which item to get
     * @return The bitmap if found in cache, null otherwise
     */
    public Bitmap getBitmapFromMemCache(String data) {
        //BEGIN_INCLUDE(get_bitmap_from_mem_cache)
        Bitmap memValue = null;

        if (bitmapLruCache != null) {
            memValue = bitmapLruCache.get(data);
        }

        if (BuildConfig.DEBUG && memValue != null) {
            Log.d(TAG, "GET cache hit");
        }
        Log.d(TAG, "**************************GET cache hit" + memValue);

        return memValue;
        //END_INCLUDE(get_bitmap_from_mem_cache)
    }
}
