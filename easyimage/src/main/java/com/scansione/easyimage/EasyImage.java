package com.scansione.easyimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.Toast;

import com.scansione.easyimage.helper.RequestMaker;
import com.scansione.easyimage.logger.Logger;

import java.io.File;

/**
 * Created by ajijul on 21/12/16.
 */

public class EasyImage {
    private static EasyImage singletoneClass;

    public static EasyImage getInstance() {
        singletoneClass = new Builder().build();
        return singletoneClass;
    }

    public EasyImage() {
    }


    public RequestMaker fetch(String path) {
        if (path == null || path.equalsIgnoreCase("")) {
            Logger.showDebugLog("Invalid Path");
            return new RequestMaker(singletoneClass, null, 0);

        }
        return new RequestMaker(singletoneClass, Uri.parse(path), 0);

    }

    public RequestMaker fetch(File file) {
        if (file == null) {
            Logger.showDebugLog("File not be null");
            return new RequestMaker(singletoneClass, null, 0);
        }
        return new RequestMaker(singletoneClass, Uri.fromFile(file), 0);
    }

    public RequestMaker fetch(int resourceId) {
        if (resourceId == 0) {
            Logger.showDebugLog("Resourse not be zero");
            return new RequestMaker(singletoneClass, null, 0);
        }
        return new RequestMaker(singletoneClass, null, resourceId);
    }


    public static class Builder {

        public Builder() {

        }

        public EasyImage build() {
            return new EasyImage();
        }
    }

    public interface SetOnImageLoadListener {
        void onImageLoading(int progress);

        void onImageLoadFailed(String reason);

        void onImageLoadCompleted(Bitmap bitmap);
    }
}
