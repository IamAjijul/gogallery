package com.scansione.gogallery.adapters;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Karmick Solution Pvt. Ltd. on 30/8/16.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    List<Fragment> fragments = new ArrayList<>();
    ViewPager viewPager;
    TabLayout tabLayout;
    List<String> title = new ArrayList<>();
    List<Integer> icons = new ArrayList<>();
    private SparseArray<Fragment> registeredFragments =new SparseArray<Fragment>();
    public ViewPagerAdapter(FragmentManager fm, ViewPager viewPager, TabLayout _tabLayout) {
        super(fm);
        this.viewPager = viewPager;
        this.tabLayout = _tabLayout;
        this.tabLayout.setupWithViewPager(viewPager);
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < icons.size(); i++) {
                    TextView tv = (TextView) (((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(i)).getChildAt(1));
                    tv.setCompoundDrawablesWithIntrinsicBounds(icons.get(i), 0, 0, 0);
                    tv.setCompoundDrawablePadding(12);
                }
            }
        });

    }


    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {


        return title.get(position);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
    public void setFragment(Fragment fragment, String title, int drawable) {
        fragments.add(fragment);
        this.title.add(title);
        if (drawable != 0)
            icons.add(drawable);
    }
}
