package com.scansione.gogallery.adapters;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scansione.easyimage.EasyImage;
import com.scansione.easyimage.logger.Logger;
import com.scansione.gogallery.R;
import com.scansione.gogallery.interfaces.SetOnItemClickListener;
import com.scansione.gogallery.model_classes.BaseModel;
import com.scansione.gogallery.viewholders.LandingRowHolder;

import java.util.ArrayList;


/**
 * Created by AJIJUL on 12/3/2016.
 */

public class LandingRecyclerAdapter extends RecyclerView.Adapter<LandingRowHolder> {
    private ArrayList<BaseModel> baseModels;
    private Context context;
    private SetOnItemClickListener listener;

    public LandingRecyclerAdapter(ArrayList<BaseModel> baseModels, Context activity,
                                  SetOnItemClickListener listener) {
        this.baseModels = baseModels;
        this.context = activity;
        this.listener = listener;
    }

    @Override
    public LandingRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LandingRowHolder(LayoutInflater.from(context).inflate(R.layout.row_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(final LandingRowHolder holder, final int position) {

        BaseModel baseModel = baseModels.get(position);
        String pathRequest = baseModel.getUser().getProfile_image().getLarge();
        holder.rowRecycler_imv.setImageDrawable(null);
        Logger.setLogTag("JIIII");
        Logger.showErrorLog("URL" + pathRequest);
        EasyImage.getInstance().fetch(pathRequest).into(holder.rowRecycler_imv, null);
        holder.rowRecycler_imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position, holder.rowRecycler_imv);
            }
        });

    }

    @Override
    public int getItemCount() {
        return baseModels.size();
    }

    public void notifyMyAdapter(ArrayList<BaseModel> baseModels) {
        this.baseModels = new ArrayList<>(baseModels);
        notifyDataSetChanged();
    }
}
