package com.scansione.gogallery.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.scansione.easyimage.EasyImage;
import com.scansione.gogallery.R;
import com.scansione.gogallery.helpers.Logger;
import com.scansione.gogallery.interfaces.SetOnItemClickListener;
import com.scansione.gogallery.model_classes.BaseModel;
import com.scansione.gogallery.tables.ImageTable;
import com.scansione.gogallery.viewholders.ImageRowHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AJIJUL on 12/3/2016.
 */

public class ImageListingRecyclerAdapter extends RecyclerView.Adapter<ImageRowHolder> {
    private List<ImageTable> imageTables;
    private Context context;
    private SetOnItemClickListener listener;

    public ImageListingRecyclerAdapter(List<ImageTable> imageTables, Context context,
                                       SetOnItemClickListener listener) {
        this.imageTables = imageTables;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ImageRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageRowHolder(LayoutInflater.from(context).inflate(R.layout.row_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(final ImageRowHolder holder, final int position) {

        ImageTable imageModel = imageTables.get(position);
        String pathRequest = imageModel.getImagePath();
        Logger.setLogTag("Adapter");
        Logger.showErrorLog(context + " URL " + pathRequest);
        EasyImage.getInstance().fetch(pathRequest).into(holder.rowRecycler_imv, null);
        holder.rowRecycler_imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position, holder.rowRecycler_imv);
            }
        });

    }

    @Override
    public int getItemCount() {
        return imageTables.size();
    }

    public void notifyMyAdapter(List<ImageTable> imageTables) {
        this.imageTables = new ArrayList<>(imageTables);
        notifyDataSetChanged();
    }
}
