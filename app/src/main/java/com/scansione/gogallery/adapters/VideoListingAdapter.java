package com.scansione.gogallery.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scansione.easyimage.EasyImage;
import com.scansione.gogallery.R;
import com.scansione.gogallery.helpers.Logger;
import com.scansione.gogallery.interfaces.SetOnItemClickListener;
import com.scansione.gogallery.tables.VideoTable;
import com.scansione.gogallery.viewholders.VideoRowHolder;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.path;

/**
 * Created by ajijul on 23/12/16.
 */

public class VideoListingAdapter extends RecyclerView.Adapter<VideoRowHolder> {

    private List<VideoTable> videoTable;
    private Context context;
    private SetOnItemClickListener listener;

    public VideoListingAdapter(List<VideoTable> videoTables, Context context,
                               SetOnItemClickListener listener) {
        this.videoTable = videoTables;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public VideoRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VideoRowHolder(LayoutInflater.from(context).inflate(R.layout.row_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(final VideoRowHolder holder, final int position) {

        VideoTable videoTable = this.videoTable.get(position);
        String pathRequest = videoTable.getVideoPath();
        Logger.setLogTag("Adapter");
        Logger.showErrorLog(context + " URL " + pathRequest);
        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(pathRequest,
                MediaStore.Images.Thumbnails.MINI_KIND);
        holder.rowRecycler_imv.setImageBitmap(thumb);
        holder.rowRecycler_imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position, holder.rowRecycler_imv);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoTable.size();
    }

    public void notifyMyAdapter(List<VideoTable> imageTables) {
        this.videoTable = new ArrayList<>(imageTables);
        notifyDataSetChanged();
    }
}
