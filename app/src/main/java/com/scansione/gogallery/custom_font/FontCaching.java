package com.scansione.gogallery.custom_font;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 * Created by ajijul on 20/12/16.
 */

public class FontCaching {
    private static HashMap<String, Typeface> fontCache = new HashMap<>();

    public static Typeface getTypeface(String fontname, Context context) {
        Typeface typeface = fontCache.get(fontname);

        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontname);
            } catch (Exception e) {
                return null;
            }

            fontCache.put(fontname, typeface);
        }

        return typeface;
    }
}
