package com.scansione.gogallery.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.scansione.gogallery.R;

/**
 * Created by ajijul on 23/12/16.
 */

public class VideoRowHolder extends RecyclerView.ViewHolder {
    public ImageView rowRecycler_imv;


    public VideoRowHolder(View itemView) {
        super(itemView);
        rowRecycler_imv = (ImageView) itemView.findViewById(R.id.rowRecycler_imv);
    }
}
