package com.scansione.gogallery.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by ajijul on 23/12/16.
 */
@DatabaseTable (tableName = "VideoTable")
public class VideoTable {
    @DatabaseField(canBeNull = false)
    String videoPath;
    @DatabaseField(canBeNull = false, id = true)
    int id;


    public VideoTable() {
    }

    public VideoTable(String imagePath, int id) {
        this.videoPath = imagePath;
        this.id = id;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
