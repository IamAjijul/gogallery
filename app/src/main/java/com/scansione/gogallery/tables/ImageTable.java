package com.scansione.gogallery.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by ajijul on 20/12/16.
 */
@DatabaseTable(tableName = "ImageTable")
public class ImageTable {

    @DatabaseField(canBeNull = false)
    String imagePath;
    @DatabaseField(canBeNull = false, id = true)
    int id;


    public ImageTable() {
    }

    public ImageTable(String imagePath, int id) {
        this.imagePath = imagePath;
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
