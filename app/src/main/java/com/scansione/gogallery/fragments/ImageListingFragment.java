package com.scansione.gogallery.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.dao.Dao;
import com.scansione.gogallery.R;
import com.scansione.gogallery.adapters.ImageListingRecyclerAdapter;
import com.scansione.gogallery.adapters.LandingRecyclerAdapter;
import com.scansione.gogallery.gateway.NetworkRequest;
import com.scansione.gogallery.interfaces.SetOnItemClickListener;
import com.scansione.gogallery.model_classes.BaseModel;
import com.scansione.gogallery.ormlite.GetDaoHelper;
import com.scansione.gogallery.tables.ImageTable;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajijul on 20/12/16.
 */

public class ImageListingFragment extends BaseFragment implements SetOnItemClickListener {
    private View view;
    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private ImageListingRecyclerAdapter adapter;
    private LandingRecyclerAdapter adapter2;
    private Dao<ImageTable, Integer> imageDao;
    private List<ImageTable> imageTables;
    private ArrayList<BaseModel> myList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_imagelisting, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        recyclerView = (RecyclerView) view.findViewById(R.id.fragmentImage_recyclerView);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        imageDao = GetDaoHelper.getOrmLiteHelper(getContext()).getImageTableDao();
        try {
            imageTables = imageDao.queryForAll();
            adapter = new ImageListingRecyclerAdapter(imageTables, getActivity(), this);
            adapter2 = new LandingRecyclerAdapter(myList, getActivity(), this);
            recyclerView.setAdapter(adapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        fetchDataFromServer();
    }

    @Override
    public void onItemClick(int position, ImageView imageView) {

    }

    private void fetchDataFromServer() {
        NetworkRequest networkRequest = NetworkRequest.getInstance(getContext());
        networkRequest.strReqWithLoader(getContext(), "http://pastebin.com/raw/wgkJgazE", "ImageListingFragment", new NetworkRequest.NetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Type listType = new TypeToken<List<BaseModel>>() {
                    }.getType();
                    Gson gson = new Gson();
                    myList = gson.fromJson(jsonArray.toString(), listType);
                    ArrayList<BaseModel> baseModels = new ArrayList<BaseModel>(myList);
                    baseModels.addAll(myList);
                    baseModels.addAll(myList);
                    baseModels.addAll(myList);
                    baseModels.addAll(myList);
                    adapter2.notifyMyAdapter(baseModels);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }
}
