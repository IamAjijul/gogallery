package com.scansione.gogallery.model_classes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AJIJUL on 12/3/2016.
 */

public class UserModel implements Parcelable {
    String id,username,name;
    ImageModel profile_image;

    protected UserModel(Parcel in) {
        id = in.readString();
        username = in.readString();
        name = in.readString();
        profile_image = in.readParcelable(ImageModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(username);
        dest.writeString(name);
        dest.writeParcelable(profile_image, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ImageModel getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(ImageModel profile_image) {
        this.profile_image = profile_image;
    }
}

/*
"user":{
        "id":"OevW4fja2No",
        "username":"nicholaskampouris",
        "name":"Nicholas Kampouris",
        */