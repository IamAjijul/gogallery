package com.scansione.gogallery.helpers;

/**
 * Created by ajijul on 20/12/16.
 */

public class Constant {
    public static final int IMAGE_LOADER_ID = 121;
    public static final int VIDEO_LOADER_ID = 122;
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 212;
    public static String DATABASE_NAME = "GoGalleryDb";
    public static final int DATABASE_VERSION = 1;
}
