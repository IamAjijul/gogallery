package com.scansione.gogallery.helpers;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;


/**
 * Copyright (C) 2016 Karmick Solution Pvt. Ltd.
 */
public class AppUtils {
    /*private static Context context = null;

    *//**
     * Recently set context will be returned.
     * If not set it from current class it will
     * be null.
     *
     * @return Context
     *//*
    public static final Context getContext() {
        return AppUtils.context;
    }

    *//**
     * First set context from every activity
     * before use any static method of AppUtils class.
     *
     * @param ctx
     *//*
    public static final void setContext(Context ctx) {
        AppUtils.context = ctx;
    }*/

    /**
     * Get String from resource id
     *
     * @param res
     * @return
     */
    public static final String getStringFromResource(Context context, int res) {
        if (null != context) {
            try {
                return context.getResources().getString(res);
            } catch (Resources.NotFoundException e) {
                Logger.printStackTrace(e);
                return "";
            }
        } else {
            Logger.showErrorLog("CONTEXT null");
            return "";
        }
    }

    /**
     * Check for email validation using android default
     * email validator.
     *
     * @param target
     * @return boolean
     */
    public static final boolean isValidEmail(CharSequence target) {
        if (target == null || target.equals("")) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivity(Context context, Class cls) {
        if (null != context) {
            Intent intent = new Intent(context, cls);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        } else {
            Logger.showErrorLog("CONTEXT null");
        }
    }


    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivityWithSingleExtra(Context context, Class cls, String stringExtra) {
        if (null != context) {
            Intent intent = new Intent(context, cls);
            intent.putExtra("extra", stringExtra);
            context.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        } else {
            Logger.showErrorLog("CONTEXT null");
        }
    }

    public static int getStatusBarHeight(Activity mActivity) {
        Rect rect = new Rect();
        Window win = mActivity.getWindow();
        win.getDecorView().getWindowVisibleDisplayFrame(rect);

        int statusBarHeight = rect.top;
        int contentViewTop = win.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = contentViewTop - statusBarHeight;
        if (statusBarHeight == 0) {
            statusBarHeight = 40;
        }
        return statusBarHeight;
    }


    public static final void hideSoftInputMode(final Context context, final View et) {
        if (null != context) {
            InputMethodManager im = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(et.getWindowToken(), 0);
        } else {
            Logger.showErrorLog("CONTEXT null");
        }
    }

    public static boolean isExternalStoragePresent(Context context) {
        Boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        return isSDPresent;

    }


    public static boolean isMyServiceRunningTest(Context _context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) _context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            Logger.showDebugLog("service.service.getClassName() - " + service.service.getClassName());
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void setBrightness(Activity context, float brightness) {

        WindowManager.LayoutParams lp = context.getWindow().getAttributes();
        lp.screenBrightness = brightness;
        context.getWindow().setAttributes(lp);

    }


}
