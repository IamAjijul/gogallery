package com.scansione.gogallery.ormlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.scansione.gogallery.helpers.Constant;
import com.scansione.gogallery.helpers.Logger;
import com.scansione.gogallery.tables.ImageTable;
import com.scansione.gogallery.tables.VideoTable;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Database helper which creates and upgrades the database and provides the DAOs for the app.
 */
public class OrmLiteDatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final AtomicInteger usageCounter = new AtomicInteger(0);

    // the DAO object we use to access the ComplexData table
    // we do this so there is only one helper
    private static OrmLiteDatabaseHelper helper = null;
    /************************************************
     * Suggested Copy/Paste code. Everything from here to the done block.
     ************************************************/

    // ImageTable dao [starts]
    private Dao<ImageTable, Integer> imageTableDao;
    //VideoTable Dao
    private Dao<VideoTable, Integer> videoTableDao;


    public OrmLiteDatabaseHelper(Context context) {
        super(context, Constant.DATABASE_NAME, null, Constant.DATABASE_VERSION);
        Logger.showDebugLog("_______________OrmLiteDatabaseHelper_______________");

    }

    /**
     * Get the helper, possibly constructing it if necessary. For each call to this method, there should be 1 and only 1
     * call to {@link #close()}.
     */
    public static synchronized OrmLiteDatabaseHelper getHelper(Context context) {
        if (helper == null) {
            helper = OpenHelperManager.getHelper(context, OrmLiteDatabaseHelper.class);
        }
        usageCounter.incrementAndGet();
        return helper;
    }

    /************************************************
     * Suggested Copy/Paste Done
     ************************************************/

    @Override
    public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
        try {
            // Create tables. This onCreate() method will be invoked only once of the application life time i.e. the first time when the application starts.
            // Profile section dao [starts]
            TableUtils.createTable(connectionSource, ImageTable.class);
            TableUtils.createTable(connectionSource, VideoTable.class);

            // Lecture section dao [ends]


        } catch (SQLException e) {
            Log.e(OrmLiteDatabaseHelper.class.getName(), "Unable to create datbases", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
        try {

            // In case of change in database of next version of application, please increase the value of DATABASE_VERSION variable, then this method will be invoked
            //automatically. Developer needs to handle the upgrade logic here, i.e. create a new table or a new column to an existing table, take the backups of the
            // existing database etc.

            TableUtils.dropTable(connectionSource, ImageTable.class, true);
            TableUtils.dropTable(connectionSource, VideoTable.class, true);

            // Lecture section dao [ends]


            onCreate(sqliteDatabase, connectionSource);

        } catch (SQLException e) {
            Log.e(OrmLiteDatabaseHelper.class.getName(), "Unable to upgrade database from version " + oldVer + " to new "
                    + newVer, e);
        }
    }

    // Create the getDao methods of all database tables to access those from android code.
    // Insert, delete, read, update everything will be happened through DAOs

    // Profile section dao [starts]
    public Dao<ImageTable, Integer> getImageTableDao() {
        try {
            if (imageTableDao == null) {
                imageTableDao = getDao(ImageTable.class);
            }
        } catch (SQLException e) {
            Logger.printStackTrace(e);
        }
        return imageTableDao;
    }


    // Lecture section dao [ends]

    // Lecture section dao [ends]

    /**
     * Close the database connections and clear any cached DAOs. For each call to {@link #getHelper(Context)}, there
     * should be 1 and only 1 call to this method. If there were 3 calls to {@link #getHelper(Context)} then on the 3rd
     * call to this method, the helper and the underlying database connections will be closed.
     */
    @Override
    public void close() {
        if (usageCounter.decrementAndGet() == 0) {
            super.close();
            helper = null;
        }
    }

    public void createTableManually(Context context) {
        try {
            TableUtils.createTableIfNotExists(connectionSource,
                    ImageTable.class);


            connectionSource.close();
/*
            GetDaoHelper.destroyHelpers(context);
*/


        } catch (SQLException e) {
            Logger.printStackTrace(e);
        }
    }


    public void dropTableManually(Context context) {
        try {
            TableUtils.dropTable(connectionSource,
                    ImageTable.class, true);

            connectionSource.close();
            GetDaoHelper.destroyHelpers(context);


        } catch (SQLException e) {
            Logger.printStackTrace(e);
        }
    }

    public Dao<VideoTable, Integer> getVideoTableDao() {
        try {
            if (videoTableDao == null)
                videoTableDao = getDao(VideoTable.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return videoTableDao;

    }
}
