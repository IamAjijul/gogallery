package com.scansione.gogallery.activities;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.j256.ormlite.dao.Dao;
import com.scansione.gogallery.R;
import com.scansione.gogallery.helpers.AppUtils;
import com.scansione.gogallery.helpers.Constant;
import com.scansione.gogallery.helpers.Logger;
import com.scansione.gogallery.helpers.MarshmallowPermissionHelper;
import com.scansione.gogallery.ormlite.GetDaoHelper;
import com.scansione.gogallery.ormlite.OrmLiteDatabaseHelper;
import com.scansione.gogallery.tables.ImageTable;
import com.scansione.gogallery.tables.VideoTable;

import java.sql.SQLException;

/**
 * Created by ajijul on 20/12/16.
 */

public class SplashScreen extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private Dao<ImageTable, Integer> imageTableDao;
    private Dao<VideoTable, Integer> videoTableDao;
    private OrmLiteDatabaseHelper ormLiteDatabaseHelper;
    private ProgressBar splashScreen_progress;
    private Handler delayHandler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        initViews();
        if (MarshmallowPermissionHelper.getStorageAndCameraPermission(this, null, this,
                Constant.STORAGE_PERMISSION_REQUEST_CODE)) {
            initClassVariables();
        }

    }

    private void initViews() {
        splashScreen_progress = (ProgressBar) findViewById(R.id.splashScreen_progress);
    }

    private void initClassVariables() {

        Logger.showDebugLog("IinitClassViewAndVariables");
        ormLiteDatabaseHelper = GetDaoHelper.getOrmLiteHelper(this);
        imageTableDao = ormLiteDatabaseHelper.getImageTableDao();
        videoTableDao = ormLiteDatabaseHelper.getVideoTableDao();
        getSupportLoaderManager().initLoader(Constant.IMAGE_LOADER_ID, null, this);

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Logger.showDebugLog("onCreateLoader");
        CursorLoader myCursor = null;
        switch (id) {
            case Constant.IMAGE_LOADER_ID:
                final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
                final String orderBy = MediaStore.Images.Media.DATE_MODIFIED;
                myCursor = new CursorLoader(this, MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        columns, null, null, orderBy);
                break;
            case Constant.VIDEO_LOADER_ID:
                final String[] videoColumns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID};
                final String videoOrderBy = MediaStore.Video.Media.DATE_MODIFIED;
                myCursor = new CursorLoader(this, MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        videoColumns, null, null, videoOrderBy);
                break;
        }
        return myCursor;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Logger.showDebugLog("onLoadFinished");

        switch (loader.getId()) {
            case Constant.IMAGE_LOADER_ID:
                if (data != null && data.getCount() > 0) {
                    int totalCount = data.getCount();
                    final float stepSize = (100.0f / totalCount);
                    for (int i = 0; i < totalCount; i++) {
                        final int progress = i + 1;
                        data.moveToPosition(i);
                        try {
                            splashScreen_progress.setProgress(Math.round(progress * stepSize));
                            imageTableDao.createIfNotExists(new ImageTable(data.getString(data.getColumnIndex(MediaStore.Images.Media.DATA)),
                                    data.getInt(data.getColumnIndex(MediaStore.Images.Media._ID))));
                        } catch (SQLException e) {
                            Logger.showDebugLog("SQLException During Image Path Save to Database");
                            e.printStackTrace();
                        }
                    }
                }
                splashScreen_progress.setProgress(0);
                getSupportLoaderManager().initLoader(Constant.VIDEO_LOADER_ID, null, this);
                break;
            case Constant.VIDEO_LOADER_ID:

                if (data != null && data.getCount() > 0) {
                    int totalCount = data.getCount();
                    final float stepSize = (100.0f / totalCount);
                    for (int i = 0; i < totalCount; i++) {
                        final int progress = i + 1;
                        data.moveToPosition(i);
                        try {
                            splashScreen_progress.setProgress(Math.round(progress * stepSize));
                            videoTableDao.createIfNotExists(new VideoTable(data.getString(data.getColumnIndex(MediaStore.Video.Media.DATA)),
                                    data.getInt(data.getColumnIndex(MediaStore.Video.Media._ID))));
                        } catch (SQLException e) {
                            Logger.showDebugLog("SQLException During Video Path Save to Database");
                            e.printStackTrace();
                        }
                    }
                }
                GetDaoHelper.destroyHelpers(this);
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AppUtils.redirectActivity(SplashScreen.this, LandingActivity.class);
                    }
                }, 1000);

                break;
        }


    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Logger.showDebugLog("onLoaderReset");
        getSupportLoaderManager().restartLoader(Constant.IMAGE_LOADER_ID, null, this);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    initClassVariables();
                }
                break;
        }
    }
}
