package com.scansione.gogallery.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.scansione.gogallery.R;
import com.scansione.gogallery.adapters.ViewPagerAdapter;
import com.scansione.gogallery.fragments.ImageListingFragment;
import com.scansione.gogallery.fragments.VideoListingFragment;

public class LandingActivity extends AppCompatActivity {
    private TabLayout activityLanding_tabLayout;
    private ViewPager activityLanding_viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        initViews();
    }

    private void initViews() {
        activityLanding_tabLayout = (TabLayout) findViewById(R.id.activityLanding_tabLayout);
        activityLanding_viewPager = (ViewPager) findViewById(R.id.activityLanding_viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), activityLanding_viewPager, activityLanding_tabLayout);
        viewPagerAdapter.setFragment(new ImageListingFragment(), getResources().getString(R.string.photo), R.drawable.google_photos);
        viewPagerAdapter.setFragment(new VideoListingFragment(), getResources().getString(R.string.videos), R.drawable.video);
        activityLanding_viewPager.setAdapter(viewPagerAdapter);
    }
}
